defmodule PhxDeploy.PageController do
  use PhxDeploy.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
