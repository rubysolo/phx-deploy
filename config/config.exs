# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :phx_deploy,
  ecto_repos: [PhxDeploy.Repo]

# Configures the endpoint
config :phx_deploy, PhxDeploy.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "b42GC1+ZOG1XFLc3kn9yc0QxwkK9Pl6g6TTGjBqZen38X58pqia19h8tkGYzRLox",
  render_errors: [view: PhxDeploy.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PhxDeploy.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
